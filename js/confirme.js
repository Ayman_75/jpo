var cpt  = 0;
function valider() {

    var rep = document.getElementById("answer").value; // la valeur du champs
    var correc = document.getElementById("correction");
    var expl = document.getElementById("explication");


    // Quand c'est vrai 
    if(rep == "spécialisé") {
        correc.innerHTML = "Bravo champion , passe à la question suivante";        
        expl.innerHTML = "";
    }

    else if(rep == ""){
        correc.innerHTML = "Entre du texte";
        expl.innerHTML = "";
    }
    
    else {
        cpt += 1;
        if (cpt>1){
            correc.innerHTML = "Mauvaise réponse c'était : 'spécialisé' , passe à la question suivante."; 
            expl.innerHTML = "Explication : Tu devais utiliser la fonction recherche du navigateur en faisant CTRL + F et ton mot";
        }
        else{
            expl.innerHTML = "Tu ne dois pas chercher mot par mot, essaye de trouver une méthode de recherche, il te reste une chance."
        }
    }


} 
var cpt2  = 0;
function valider2() {

    var rep = document.getElementById("answer2").value; // la valeur du champs
    var correc = document.getElementById("correction2");
    var expl = document.getElementById("explication2");


    // Quand c'est vrai 
    if(rep == "Nepenthes lowii") {
        correc.innerHTML = "Félicitation ! Tu peux maintenant passer au niveau Expert.";        
        expl.innerHTML = "";
    }

    else if(rep == ""){
        correc.innerHTML = "Entre du texte";
        expl.innerHTML = "";
    }
    
    else {
        cpt2 += 1;
        if (cpt2>1){
            correc.innerHTML = "Dommage, la réponse était : Nepenthes lowii."; 
            expl.innerHTML = "Explication : Tu devais importer l'image dans Google Image et retrouver le nom de la plante, passe à la question suivante";
        }
        else{
            expl.innerHTML = "Google est ton ami et pas seulement pour des recherches de texte. Il te reste une dernière chance.";
            }
    }


} 

var cpt  = 0;
function valider() {

    var rep = document.getElementById("answer").value; // la valeur du champs
    var correc = document.getElementById("correction");
    var expl = document.getElementById("explication");


    // Quand c'est vrai 
    if(rep == "#00ff3e" || rep == "#00FF3e" || rep == "#00FF3E" || rep == "#00ff3E") {
        correc.innerHTML = "Bonne réponse ! Encore une, tu peux le faire.";        
        expl.innerHTML = "";
    }

    else if(rep == ""){
        correc.innerHTML = "Entre du texte";
        expl.innerHTML = "";
    }
    
    else {
        cpt += 1;
        if (cpt>1){
            correc.innerHTML = "Mauvaise réponse, c'était : #00ff3e. Tu peux passer à la question suivante."; 
            expl.innerHTML = "Explication : Si tu fais un clic droit, et que tu vas dans l'inspecteur, tu peux cliquer en haut à gauche de l'inspecteur puis sur l'élément dont tu souhaites le code. Il te suffit alors de regarder la case style pour voire les différentes propriétées de cette élément et dedans tu trouveras le code de la couleur. ";
        }
        else{
            correc.innerHTML = "";
            expl.innerHTML = "Il doit bien exister un inspecteur ?? Il te reste une chance."
        }
    }


} 
var cpt2  = 0;
function valider2() {

    var rep = document.getElementById("answer2").value; // la valeur du champs
    var correc = document.getElementById("correction2");
    var expl = document.getElementById("explication2");


    // Quand c'est vrai 
    if(rep == "Bravo") {
        correc.innerHTML = "Bravo champion , tu as fini ce MOOC !";        
        expl.innerHTML = "";
    }

    else if(rep == ""){
        correc.innerHTML = "Entre du texte";
        expl.innerHTML = "";
    }
    
    else {
        cpt2 += 1;
        if (cpt2>1){
            correc.innerHTML = "Dommage, la réponse était : 'Bravo' ."; 
            expl.innerHTML = "Explication : Tu devais chercher dans le footer du html.";
        }
        else{
            expl.innerHTML = "Mauvaise réponse, fait attention il te reste une dernière chance.";
            }
    }


} 
